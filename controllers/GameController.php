<?php

namespace app\controllers;

use Yii;
use app\models\Game;
use app\models\Comment;
use app\models\Resource;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\VarDumper;
use yii\data\Pagination;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Game models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Game::find(),
        ]);

        return $this->render('frontpage', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMyGames() {
        if(!Yii::$app->user->can('createGame')) {
            throw new \yii\web\ForbiddenHttpException;
        }
        
        $userId = Yii::$app->user->id;
        
        $dataProvider = new ActiveDataProvider([
            'query' => Game::find()->where(['users_id' => $userId]),
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Game model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'comment' => new Comment(),
        ]);
    }

    /**
     * Creates a new Game model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!Yii::$app->user->can('createGame')) {
            throw new \yii\web\UnauthorizedHttpException;
        }
        
        $model = new Game();
        
        $isValid = $model->load(Yii::$app->request->post());
        $date = new \DateTime();
        $model->create_date = $date->format('Y-m-d');
        $model->update_date = $model->create_date;
        $model->users_id = Yii::$app->user->id; // only logged in usere can create new game
        
        $model->setCoverFile(UploadedFile::getInstance($model, 'cover'));
        
        if($isValid && !$model->validate()) {
            Yii::error('XXX Error while validating user input ', __METHOD__);
            $isValid = false;
        }
        
        if ($isValid && $model->save()) {
            $uploadedFiles = UploadedFile::getInstances($model, 'resources[filename]');
            foreach($uploadedFiles as $uf) {
                $resource = new Resource();
                $resource->games_id = $model->id;
                $resource->setResourceFile($uf);

                if($resource->validate()) {
                    $resource->save();
                }
            }
            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Game model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if(!Yii::$app->user->can('updateGame', ['game' => $model])) {
            throw new \yii\web\UnauthorizedHttpException;
        }
        
        $isValid = $model->load(Yii::$app->request->post());
        
        $date = new \DateTime();
        $model->update_date = $date->format('Y-m-d');
        
        $model->setCoverFile(UploadedFile::getInstance($model, 'cover'));
        
        $uploadedFiles = UploadedFile::getInstances($model, 'resources[filename]');
        foreach($uploadedFiles as $uf) {
            $resource = new Resource();
            $resource->games_id = $id;
            $resource->setResourceFile($uf);
            
            if($resource->validate()) {
                $isValid = $resource->save() && $isValid;
            }
        }
        
        if($isValid && !$model->validate()) {
            Yii::error("XXX Error while validating user input ", __METHOD__);
            $isValid = false;
        }

        if ($isValid && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Game model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $game = $this->findModel($id);
        
        if(!Yii::$app->user->can('updateGame', ['game' => $game])) {
            throw new \yii\web\UnauthorizedHttpException;
        }
        
        if($game->delete()) {
            return $this->redirect(['index']);
        }
        
        return $this->goBack();
    }

    /**
     * Finds the Game model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Game the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Game::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAddComment($id) {
        
        if(!Yii::$app->user->can('createComment')) {
            throw new \yii\web\ForbiddenHttpException;
        }
        
        $comment = new Comment();
        $isValid = $comment->load(Yii::$app->request->post());
        $comment->games_id = $id;
        $comment->users_id = Yii::$app->user->id;
        
        $date = new \DateTime();
        $comment->create_date = $date->format('Y-m-d');
        
        if($isValid && !$comment->validate()) {
            Yii::error("XXX Error while validating comment", __METHOD__);
            $isValid = false;
            
            $errors = $comment->getErrors();
            VarDumper::dump($errors);
            Yii::$app->end();
        }
        
        if($isValid && $comment->save()) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            Yii::error("XXX Error while saving comment");
            return $this->goBack();
        }
        
    }
    
    public function actionDeleteResource($id) {
        $resource = Resource::findOne($id);
        if($resource === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        if(!Yii::$app->user->can('updateGame', ['game' => $resource->games])) {
            throw new \yii\web\UnauthorizedHttpException;
        }
        
        $resource->delete();        
        return $this->redirect(['update', 'id' => $resource->games_id]);
    }
}
