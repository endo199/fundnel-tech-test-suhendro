<?php
return [
    'createGame' => [
        'type' => 2,
        'description' => 'Create a Game',
    ],
    'updateGame' => [
        'type' => 2,
        'description' => 'Only the creator of the game article can update it',
        'ruleName' => 'updateGameRule',
    ],
    'createComment' => [
        'type' => 2,
        'description' => 'Only registered user can comment',
    ],
    'updateData' => [
        'type' => 2,
        'description' => 'Only owner can update s/he data',
        'ruleName' => 'updateOwnDataRule',
    ],
    'author' => [
        'type' => 1,
        'children' => [
            'createGame',
            'updateGame',
            'createComment',
            'updateData',
        ],
    ],
];
