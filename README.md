Fundnel Game Portal
============================

This project is just a simple test case on using Yii2

INSTALLATION
------------
1. Create database and import structure from fundnel-game-portal.sql
2. Update configurasi /config/db.php with database setting that you create from previous step
3. Run the web server with Document Root pointing at /web directory

NOTES
-----
1. Functional testing (GameCept.php) is related to data in database. So, you might need to update a little (eg: info about username and password) to run the tests