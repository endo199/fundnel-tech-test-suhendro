<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property string $comment
 * @property integer $games_id
 * @property integer $users_id
 * @property string $create_date
 *
 * @property Games $games
 * @property Users $users
 */
class Comment extends \yii\db\ActiveRecord
{        
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment', 'games_id', 'users_id', 'create_date'], 'required'],
            [['games_id', 'users_id'], 'integer'],
            [['comment'], 'string'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => 'Comment',
            'games_id' => 'Games ID',
            'users_id' => 'Users ID',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasOne(Game::className(), ['id' => 'games_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id']);
    }
}
