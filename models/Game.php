<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $users_id
 * @property string $cover
 * @property string $create_date
 * @property string $update_date
 *
 * @property Comments[] $comments
 * @property Users $users
 * @property Resources[] $resources
 */
class Game extends \yii\db\ActiveRecord
{
    private $coverFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'users_id', 'create_date', 'update_date'], 'required'],
            [['description'], 'string'],
            [['users_id'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['title'], 'string', 'max' => 45],
            [['coverFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'users_id' => 'Users ID',
            'cover' => 'Cover',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['games_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(Resource::className(), ['games_id' => 'id']);
    }
    
    /**
    * set uploaded file as cover file of the game. Name of the file will be generate automatically but still maintaining file extension 
    * (for new cover) or use existing name if update cover
    */
    public function setCoverFile(UploadedFile $uploadedFile = null) {
        if(isset($uploadedFile)) {
            $this->coverFile = $uploadedFile;
            
            if(empty($this->cover)) {
                $date = new \DateTime();
                $this->cover = $date->format('YmdHis'). rand() . '.' . $this->coverFile->extension;
            }
            Yii::info("XXX file name: ".$this->cover);
        } else {
            Yii::info("XXX no file uploaded");
        }
    }
    
    public function getCoverFile() {
        return $this->coverFile;
    }
    
    /**
    * upload file to the designated directory
    */
    protected function upload() {
        $uploadSuccess = true;
        
        if(isset($this->coverFile)) {
            $uploadSuccess = $this->coverFile->saveAs(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . $this->cover);
            if(!$uploadSuccess) {
                Yii::error("XXX Error while uploading file ", __METHOD__);
            }
        }
        
        return $uploadSuccess;
    }
    
    /**
    * delete cover file and all it's resources. Because once the game is deleted, all resource and comments automatically deleted
    */
    protected function deleteCoverAndResources() {
        if(isset($this->coverFile)) {
            $file = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . $this->cover;
            if(file_exists($file)) {
                unlink($file);
            }
        }
        
        foreach($this->resources as $res) {
            $file = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . $res->filename;
            if(file_exists($file)) {
                unlink($file);
            }
        }
    }
    
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            // upload the file before saving data to database
            return $this->upload();
        }
        
        return false;
    }
    
    public function afterDelete() {
        // delete cover and resources after deleting game
        $this->deleteCoverAndResources();
        parent::afterDelete();
    }
}
