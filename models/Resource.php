<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "resources".
 *
 * @property integer $id
 * @property string $filename
 * @property integer $games_id
 *
 * @property Games $games
 */
class Resource extends \yii\db\ActiveRecord
{
    private $resourceFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resources';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'games_id'], 'required'],
            [['games_id'], 'integer'],
            [['filename'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'games_id' => 'Games ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasOne(Game::className(), ['id' => 'games_id']);
    }
    
    public function getResourceFile() {
        return $this->resourceFile;
    }
    
    /**
    * Set resource from the uploaded file by user. File name automatically generated, but file extension still the same
    */
    public function setResourceFile(UploadedFile $file) {
        $this->resourceFile = $file;
        
        $date = new \DateTime();
        $this->filename = $date->format('YmdHis') . rand() . '.' . $this->resourceFile->extension;
    }
    
    /**
    * save file to the designated directory
    */
    public function upload() {
        $uploadSuccess = true;
        
        if(isset($this->resourceFile)) {
            $uploadSuccess = $this->resourceFile->saveAs(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . $this->filename);
            if(!$uploadSuccess) {
                Yii::error("XXX Error while uploading file ", __METHOD__);
            }
        }
        
        return $uploadSuccess;
    }
    
    /**
    * delete the file from file system
    */
    public function delete() {
        if(isset($this->resourceFile)) {
            $file = Yii::$app->basePath . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'uploads'. DIRECTORY_SEPARATOR . $this->filename;
            if(file_exists($file)) {
                unlink($file);
            }
        }
    }
    
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            // upload the file before saving data to database
            return $this->upload();
        }
        
        return false;
    }
    
    public function afterDelete() {
        // delete file after deleting data from database
        $this->delete();
        parent::afterDelete();
    }
}
