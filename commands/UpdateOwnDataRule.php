<?php
namespace app\commands;

use Yii;
use yii\rbac\Rule;

class UpdateOwnDataRule extends Rule {
    public function execute($user, $item, $params) {
        return $params['user']->id == $user;
    }
}