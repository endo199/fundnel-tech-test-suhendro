<?php
namespace app\commands;

use Yii;
use yii\rbac\Rule;

class UpdateGameRule extends Rule {
    public function execute($user, $item, $params) {
        return $params['game']->users_id == $user;
    }
}