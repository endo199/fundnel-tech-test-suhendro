<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller {
    public function actionInit() {
        $auth = Yii::$app->authManager;
        
        //permissions
        // create game
        $createGame = $auth->createPermission('createGame');
        $createGame->description = 'Create a Game';
        $auth->add($createGame);
        
        // rule for update
        $updateGameRule = new UpdateGameRule();
        $updateGameRule->name = 'updateGameRule';
        $auth->add($updateGameRule);
        
        // update game
        $updateGame = $auth->createPermission('updateGame');
        $updateGame->description = 'Only the creator of the game article can update it';
        $updateGame->ruleName = 'updateGameRule';
        $auth->add($updateGame);
        
        // create comment
        $createComment = $auth->createPermission('createComment');
        $createComment->description = 'Only registered user can comment';
        $auth->add($createComment);
        
        // rule for update own data
        $updateOwnDataRule = new UpdateOwnDataRule();
        $updateOwnDataRule->name = 'updateOwnDataRule';
        $auth->add($updateOwnDataRule);
        
        // update data
        $updateData = $auth->createPermission('updateData');
        $updateData->description = 'Only owner can update s/he data';
        $updateData->ruleName = 'updateOwnDataRule';
        $auth->add($updateData);
        
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createGame);
        $auth->addChild($author, $updateGame);
        $auth->addChild($author, $createComment);
        $auth->addChild($author, $updateData);
    }
}