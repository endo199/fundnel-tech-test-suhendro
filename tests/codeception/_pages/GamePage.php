<?php

namespace tests\codeception\_pages;

use yii\codeception\BasePage;

/**
 * Represents game page
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class GamePage extends BasePage
{
    public $route = 'game/create';

    public function submit(array $gameData) {
        foreach ($gameData as $field => $value) {
            $inputType = $field === 'description' ? 'textarea' : 'input';
            $this->actor->fillField($inputType . '[name="Game[' . $field . ']"]', $value);
//            $this->actor->fillField("game-title", $value);
        }
        $this->actor->click('game-button');
    }
}
