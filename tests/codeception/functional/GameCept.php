<?php

use tests\codeception\_pages\LoginPage;
use tests\codeception\_pages\GamePage;

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that create new game works');

//$identity = User::findOne(['username' => $username]);
//Yii::$app->user->login($identity);

$loginPage = LoginPage::openBy($I);

$I->amGoingTo('try to login with correct credentials');
$loginPage->login('user1', 'password');
$I->expectTo('see user info');
$I->see('Logout (User1)');

$gamePage = GamePage::openBy($I);
$I->see('Create Game', 'h1');

$I->amGoingTo('Try create a new game article');
$gamePage->submit([
    'title' => 'dummy',
    'description' => 'dummy description'
]);
$I->expectTo('see game info');
$I->see('dummy', 'h1');
