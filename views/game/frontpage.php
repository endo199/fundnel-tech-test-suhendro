<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="frontpage" class="game-index">    
    <div class="row">
        <?php foreach($dataProvider->getModels() as $game) { ?>
        <div class="col-xs-6 col-md-3">
            <div class="thumbnail">
                <img src="/uploads/<?= $game->cover ?>" alt="<?= $game->title ?>" />
                <h3><a href="<?= Url::to(['/game/view', 'id' => $game->id]) ?>"><?= Html::encode($game->title) ?></a></h3>
            </div>
        </div>
        <?php } ?>
    </div>
<!--
    <div class="grid">
        <?php foreach($dataProvider->getModels() as $game) { ?>
        <div class="grid-item">
            <div class="thumbnail">
                <img src="/uploads/<?= $game->cover ?>" alt="<?= $game->title ?>" />
                <h3><a href="<?= Url::to(['/game/view', 'id' => $game->id]) ?>"><?= Html::encode($game->title) ?></a></h3>
            </div>
        </div>
        <?php } ?>
    </div>
-->
    <?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]) ?>
</div>
