<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="game-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'cover')->fileInput() ?>
    
    <h2>Resources</h2>
    <?= $form->field($model, 'resources[filename][]')->fileInput() ?>
    <?= $form->field($model, 'resources[filename][]')->fileInput() ?>
    <?= $form->field($model, 'resources[filename][]')->fileInput() ?>
    <?= $form->field($model, 'resources[filename][]')->fileInput() ?>
    <?= $form->field($model, 'resources[filename][]')->fileInput() ?>
    
    <?php if(count($model->resources) > 0) { ?>
    <div class="row">
        <div class="col-xs-6 col-md-3">
            <?php $resources = $model->resources; ?>
            <?php foreach($resources as $res) { ?>
                <div class="thumbnail"><img src="/uploads/<?= $res->filename ?>" /><a href="<?= Url::to(['game/delete-resource', 'id' => $res->id]) ?>"><span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span></a></div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name' => 'game-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
