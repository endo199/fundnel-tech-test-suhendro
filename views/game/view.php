<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Game */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-view">

    <h1><?= $model->title ?></h1>
    <div class="row">
        <div class="col-md-8">
            <div id="gallery">
            <?php foreach($model->resources as $resource) {
            ?>
                <div><img src="/uploads/<?= $resource->filename ?>" /></div>
            <?php
            } ?>
            </div>
        </div>
        <div id="game" class="col-md-4">
            <img src="/uploads/<?= $model->cover ?>" title="<?= $model->title ?>" class="img-responsive"/>
            <p class="description"><?= nl2br(Html::encode($model->description)) ?></p>
        </div>
    </div>
    
    <h2>Leave a Comment</h2>
    <?php if(!Yii::$app->user->isGuest) { ?>
    <div class="comment-form">
        <?php $form = ActiveForm::begin(['action' => ['game/add-comment', 'id' => $model->id]]); ?>
            <?= $form->errorSummary($comment); ?>
            <?= $form->field($comment, 'comment')->textarea(['rows' => 6])->label(false) ?>
            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
            </div>
        
        <?php ActiveForm::end(); ?>
    </div>
    <?php } else { ?>
        <a href="<?= Url::to('site/login') ?>">Login to Leave a Comment</a>
    <?php } ?>
    
    <h2>Comments</h2>
    <div class="row">
        <?php foreach($model->comments as $comment) { ?>
            <div class="comment col-md-8">
                <h4><?= $comment->users->name ?></h4>
                <em><?= $comment->create_date ?></em>
                <p><?= nl2br(Html::encode($comment->comment)) ?></p>
            </div>
        <?php } ?>
    </div>
    
    

</div>