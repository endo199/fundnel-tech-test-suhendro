<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css',
        'http://cdn.jsdelivr.net/jquery.slick/1.5.9/slick-theme.css',
        'css/site.css',
    ];
    public $js = [
        'http://code.jquery.com/jquery-migrate-1.2.1.js',
        'http://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js',
        'js/site.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
